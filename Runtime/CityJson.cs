﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Numerics;

namespace Wander
{
    // Docs: https://www.cityjson.org/dev/geom-arrays/
    // FastJSON: https://www.codeproject.com/Articles/159450/fastJSON-Smallest-Fastest-Polymorphic-JSON-Seriali
    // Optional not used UTF8Json (2 times slower than FastJSON): https://github.com/neuecc/Utf8Json 
    // NewtonSoft.Json avoided, a factor 3 times slower than FastJSON at the cost of more memory consumption.

    public enum GeometryType
    {
        MultiSurface,
        CompositeSurface,
        Solid,
        MultiSolid,
        CompositeSolid
    }

    public class CityMaterial
    {
        public string name;
        public float ambientIntensity;
        public Vector3 diffuseColor;
        public Vector3 emissiveColor;
        public Vector3 specularColor;
        public float shininess;
        public float transparency;
        public bool isSmooth;
    }

    public class CityGeometry
    {
        public GeometryType type;
        public bool hasInteriorRings;
        public int lod;
        public List<int> indices;
        public List<CityMaterial> materials;
    }

    public class CityObject
    {
        public string type; // TODO change to enum
        public CityGeometry geometry;
        // TODO add attributes
        // TODO add parent/child relation
    }

    public class CityJson
    {
        // Context info
        Stopwatch clock;
        bool swapIndices02;
        Dictionary<string, object> cityJson;

        // Temp data to avoid too much (re)allocation.
        List<double> tempVertices = new List<double>();
        List<int> tempHoleIndices = new List<int>();
        List<int> tempIndices     = new List<int>();
        List<int> geometryIndices = new List<int>();
        bool geometryHasInteriorRings;

        // Output
        string title;
        string crs;
        string referenceDate;
        Vector3 geoExtentMin;
        Vector3 geoExtentMax;
        Vector3 scale;
        Vector3 translate;
        List<(long x, long y, long z)> vertices;
        List<CityObject> cityObjects      = new List<CityObject>();
        List<CityMaterial> cityMaterials  = new List<CityMaterial>();
        Dictionary<string, long> timings  = new Dictionary<string, long>();
        Dictionary<string, int>  errors   = new Dictionary<string, int>();

        // Output properties
        public string Title => title;
        public Dictionary<string, long> Timings => timings;
        public Dictionary<string, int> Errors => errors;
        public List<(long x, long y, long z)> Vertices => vertices;
        public List<CityObject> CityObjects => cityObjects;
        public List<CityMaterial> Materials => cityMaterials;
        public Vector3 GeoExtentMin => geoExtentMin;
        public Vector3 GeoExtentMax => geoExtentMax;
        public Vector3 Scale => scale;
        public Vector3 Translate => translate;
        public string ReferenceDate => referenceDate;
        public string CRS => crs;

        public CityJson( string json, bool flipIndices02 = true )
        {
            clock = new Stopwatch();
            scale = new Vector3( 1, 1, 1 );
            swapIndices02   = flipIndices02;

            ParseJson(json);
            ReadTransform();
            ReadMetadata();
            ReadAppearence();
            ReadVertices();
            ReadCityObjects();

            // Release handles to these so GC can claim the mem.
            {
                tempVertices = null;
                tempHoleIndices = null;
                tempIndices = null;
                tempHoleIndices = null;
                geometryIndices = null;
                cityJson = null;
                clock = null;
            }
        }

        void ParseJson( string json )
        {
            clock.Restart();
            cityJson = fastJSON.JSON.Parse( json ) as Dictionary<string, object>;
            timings.Add( "Parse CityJson", clock.ElapsedMilliseconds );
        }

        void ReadTransform()
        {
            clock.Restart();
            if (cityJson.TryGetValue( "transform", out object transformAsObj ))
            {
                var transform = transformAsObj as Dictionary<string, object>;
                scale         = TryGetVector3( transform, "scale", Vector3.One );
                translate     = TryGetVector3( transform, "translate", Vector3.One );
            }
            timings.Add( "Read transform", clock.ElapsedMilliseconds );
        }

        void ReadMetadata()
        {
            clock.Restart();
            if (cityJson.TryGetValue( "metadata", out object metaDataObj ))
            {
                var metaData = metaDataObj as Dictionary<string, object>;
                if (metaData.TryGetValue( "geographicalExtent", out object geoExtent ))
                {
                    var numbers = geoExtent as List<object>;
                    geoExtentMin = new Vector3( ToFloat( numbers[0] ), ToFloat( numbers[1] ), ToFloat( numbers[2] ) );
                    geoExtentMax = new Vector3( ToFloat( numbers[3] ), ToFloat( numbers[4] ), ToFloat( numbers[5] ) );
                }
                if (metaData.TryGetValue( "referenceSystem", out object crsObj ))
                {
                    crs = crsObj as string;
                }
                if (metaData.TryGetValue( "referenceDate", out object date ))
                {
                    referenceDate = date as string;
                }
                if (metaData.TryGetValue( "title", out object tileAsObject ))
                {
                    title = tileAsObject as string;
                }
            }
            timings.Add( "Read metadata", clock.ElapsedMilliseconds );
        }

        void ReadAppearence()
        {
            clock.Restart();
            if (cityJson.TryGetValue( "appearance", out object appearenceAsObject ))
            {
                var appearence = appearenceAsObject as Dictionary<string, object>;
                if (appearence.TryGetValue( "materials", out object materialsAsObject ))
                {
                    var materials = materialsAsObject as List<object>;
                    for (int i = 0;i < materials.Count;i++)
                    {
                        HandleMaterial( materials[i] as Dictionary<string, object> );
                    }
                }
                if (appearence.TryGetValue( "textures", out object texturesAsObject ))
                {
                    // TODO
                    var textures = texturesAsObject as List<object>;
                }
            }
            timings.Add( "Read appearance", clock.ElapsedMilliseconds );
        }

        void ReadVertices()
        {
            clock.Restart();
            if (cityJson.TryGetValue( "vertices", out object verticesAsObject ))
            {
                var verticesAsList = verticesAsObject as List<object>;
                vertices = new List<(long x, long y, long z)>( verticesAsList.Count );
                for (int i = 0;i < verticesAsList.Count;i++)
                {
                    var xyzAsList = verticesAsList[i] as IList<object>;
                    long x = ToIndex( xyzAsList[0] );
                    long y = ToIndex( xyzAsList[1] );
                    long z = ToIndex( xyzAsList[2] );
                    vertices.Add( (x, y, z) );
                }
            }
            else
            {
                errors.Add( "No vertices, generation stops.", 1 );
                return;
            }
            timings.Add( "Read vertices", clock.ElapsedMilliseconds );
        }

        void ReadCityObjects()
        {
            clock.Restart();
            if (cityJson.TryGetValue( "CityObjects", out object cityObjectsAsObject ))
            {
                var cityObjects = cityObjectsAsObject as Dictionary<string, object>;
                foreach (var kvp in cityObjects)
                {
                    var ctyObjDictionary = kvp.Value as Dictionary<string, object>;

                    // Type
                    if (ctyObjDictionary.TryGetValue( "type", out object typeAsObj ))
                    {
                        CityObject ctyObj = new CityObject();
                        ctyObj.type = typeAsObj as string;

                        // Geometry
                        if (ctyObjDictionary.TryGetValue( "geometry", out object geometry ))
                        {
                            var geometries = geometry as List<object>;
                            for (int i = 0;i < geometries.Count;i++)
                            {
                                ctyObj.geometry = HandleGeometry( geometries[i] as Dictionary<string, object> );
                            }

                            this.cityObjects.Add( ctyObj );
                        }
                    }
                }
            }
            timings.Add( "Read CityObjects", clock.ElapsedMilliseconds );
        }

        void HandleMaterial( Dictionary<string, object> material )
        {
            CityMaterial ctyMat     = new CityMaterial();
            ctyMat.name             = TryGet<string>( material, "name", "" );
            ctyMat.shininess        = TryGet<float>( material, "shininess", 0.2f );
            ctyMat.ambientIntensity = TryGet<float>( material, "ambientIntensity", 0.2f );
            ctyMat.diffuseColor     = TryGetVector3( material, "diffuseColor", Vector3.One*0.7f );
            ctyMat.specularColor    = TryGetVector3( material, "specularColor", Vector3.One*0.2f );
            ctyMat.emissiveColor    = TryGetVector3( material, "emissiveColor", Vector3.Zero );
            ctyMat.transparency     = TryGet<float>( material, "transparency", 0.0f );
            cityMaterials.Add( ctyMat );
        }

        void HandleTexture( Dictionary<string, object> texture )
        {

        }

        CityGeometry HandleGeometry( Dictionary<string, object> geometry )
        {
            // LOD
            int lod = 0;
            if ( geometry.TryGetValue( "lod", out object newLodAsObject ))
            {
                int.TryParse( newLodAsObject as string, out lod );
            }

            // Type
            string type = geometry["type"] as string;
            GeometryType geomType = GeometryType.MultiSurface;

            // Boundaries
            geometryIndices = new List<int>();
            geometryHasInteriorRings = false;
            if (geometry.TryGetValue("boundaries", out object boundariesAsObject ))
            {
                var boundaries = boundariesAsObject as List<object>;
                switch (type)
                {
                    case "MultiSurface":
                        HandleMultiSurface( boundaries );
                        break;

                    case "CompositeSurface":
                        geomType = GeometryType.CompositeSurface;
                        HandleCompositeSurface( boundaries );
                        break;

                    case "Solid":
                        geomType = GeometryType.Solid;
                        HandleSolid( boundaries );
                        break;

                    case "MultiSolid":
                        geomType = GeometryType.MultiSolid;
                        HandleMultiSolid( boundaries );
                        break;

                    case "CompositeSolid":
                        geomType = GeometryType.CompositeSolid;
                        HandleCompositeSolid( boundaries );
                        break;
                }
            }

            // Material
            List<CityMaterial> geomMaterials = new List<CityMaterial>();
            if (geometry.TryGetValue( "material", out object materialAsObject ))
            {
                var materials = materialAsObject as Dictionary<string, object>;
                foreach (var mat in materials)
                {
                    var valuesDictionary = mat.Value as Dictionary<string, object>;
                    if (valuesDictionary.TryGetValue( "values", out object values ))
                    {
                        var matIndices = GetMaterialIndices( geomType, values as List<object> );
                        for ( int i = 0; i < matIndices.Count; i++ )
                        {
                            int matIdx = (int) ToIndex(matIndices[i]);
                            if (matIdx >= 0)
                            {
                                geomMaterials.Add( cityMaterials[matIdx] );
                            }
                        }
                    }
                    else if (valuesDictionary.TryGetValue( "value", out object value ))
                    {
                        int matIdx = (int) ToIndex(value);
                        if (matIdx >= 0)
                        {
                            geomMaterials.Add( cityMaterials[matIdx] );
                        }
                    }
                }
            }

            CityGeometry geom = new CityGeometry();
            geom.type      = geomType;
            geom.indices   = geometryIndices;
            geom.materials = geomMaterials;
            geom.lod       = lod;
            geom.hasInteriorRings = geometryHasInteriorRings;
            return geom;
        }

        // Surfaces may be sepearated or intersect. They do not 'belong' to each other.
        void HandleMultiSurface( List<object> boundaries )
        {
            for (int i = 0;i < boundaries.Count;i++)
            {
                var surface = boundaries[i] as List<object>;
                HandleSurface( surface );
            }
        }

        // All surfaces together are one surface.
        void HandleCompositeSurface( List<object> boundaries )
        {
            HandleMultiSurface( boundaries );
        }

        // Composed of surface collections.
        void HandleSolid( List<object> boundaries )
        {
            for (int i = 0;i < boundaries.Count;i++)
            {
                var surfaceCollection = boundaries[i] as List<object>;
                for ( int j = 0; j< surfaceCollection.Count; j++ )
                {
                    var surface = surfaceCollection[j] as List<object>;
                    HandleSurface( surface );
                }
            }
        }

        // Solids may intersect or are seperated. They do not belong to each other.
        void HandleMultiSolid( List<object> boundaries )
        {
            // each is a solid
            for (int i = 0;i < boundaries.Count;i++)
            {
                var solid = boundaries[i] as List<object>;
                HandleSolid( solid );
            }
        }

        // The solids composed form a single solid.
        void HandleCompositeSolid( List<object> boundaries )
        {
            HandleMultiSolid( boundaries );
        }

        void HandleSurface( List<object> surfaceIndices )
        {
            try
            {
                if (surfaceIndices == null || surfaceIndices.Count == 0)
                    return;

                // First surface is outerior (exterior ring)
                var outerIndices = surfaceIndices[0] as IList<object>;
                if (outerIndices.Count < 3)
                    return;

                // Obtain normal, to determine to which side to project
                int vIdx0  = (int) ToIndex( outerIndices[0] ); 
                int vIdx1  = (int) ToIndex( outerIndices[1] );
                int vIdx2  = (int) ToIndex( outerIndices[2] );
                Vector3 surfaceNormal = GetNormal(vIdx0, vIdx1, vIdx2);

                // Determine projection side
                int projectSide = 2;
                float absX = MathF.Abs(surfaceNormal.X);
                float absY = MathF.Abs(surfaceNormal.Y);
                float absZ = MathF.Abs(surfaceNormal.Z);
                if ((absX > absY) && (absX > absZ))
                {
                    projectSide = 0;
                }
                else if (absY > absZ)
                {
                    projectSide = 1;
                }

                // Build vertex list
                tempVertices.Clear();
                tempHoleIndices.Clear();
                tempIndices.Clear();
                for ( int ringIndex = 0; ringIndex < surfaceIndices.Count; ringIndex++ )
                {
                    var ring = surfaceIndices[ringIndex] as IList<object>;
                    if (ringIndex != 0 ) // Subsequent rings, not the first, are holes.
                    {
                        Debug.Assert( tempVertices.Count % 2 == 0 );
                        tempHoleIndices.Add( tempVertices.Count >> 1 );
                        geometryHasInteriorRings = true;
                    }

                    // First one is outerior, sebsequent ones are holes.
                    for ( int j = 0;j < ring.Count;j++ )
                    {
                        int vertexIndex = (int) ToIndex( ring[j] );
                        tempIndices.Add( vertexIndex ); // Remember this index to reconstruct indices.
                        AddToTempVerticesWithProjection( projectSide, vertexIndex );
                    }
                }

                // Reconstruct indices after tesselate.
                var triangulatedSurfaceIndices = EarcutNet.Earcut.Tessellate( tempVertices, tempHoleIndices );

                if (triangulatedSurfaceIndices.Count >= 3)
                {
                    // Check triangle Normal against Surface normal, if dot is negative, flip indices to preserve winding order.
                    var triNormal = GetNormal(
                        tempIndices[triangulatedSurfaceIndices[0]],
                        tempIndices[triangulatedSurfaceIndices[1]],
                        tempIndices[triangulatedSurfaceIndices[2]] 
                    );
                    bool mustSwapIndices = (Vector3.Dot( triNormal, surfaceNormal ) < 0);

                    //     if (!swapIndices02 && mustSwapIndices)
                    //         mustSwapIndices = true;

                    //      if (!swapIndices02 && !mustSwapIndices)
                    //          mustSwapIndices = false;

                    if (swapIndices02 && !mustSwapIndices)
                        mustSwapIndices = true;
                    else if (swapIndices02 && mustSwapIndices)
                        mustSwapIndices = false;

                    for (int i = 0;i < triangulatedSurfaceIndices.Count;i += 3)
                    {
                        if (!mustSwapIndices)
                        {
                            geometryIndices.Add( tempIndices[triangulatedSurfaceIndices[i+0]] );
                            geometryIndices.Add( tempIndices[triangulatedSurfaceIndices[i+1]] );
                            geometryIndices.Add( tempIndices[triangulatedSurfaceIndices[i+2]] );
                        }
                        else
                        {
                            geometryIndices.Add( tempIndices[triangulatedSurfaceIndices[i+2]] );
                            geometryIndices.Add( tempIndices[triangulatedSurfaceIndices[i+1]] );
                            geometryIndices.Add( tempIndices[triangulatedSurfaceIndices[i+0]] );
                        }
                    }
                }
            }
            catch (Exception)
            {
                // Leave out this surface, something goes wrong.
                string key = "Invalid surface";
                if (errors.TryGetValue( key, out int value ))
                    value++;
                errors[key]=value;
            }
        }

        void AddToTempVerticesWithProjection( int projectSide, int vertexIndex )
        {
            if ( projectSide == 0 )
            {
                tempVertices.Add( vertices[vertexIndex].y );
                tempVertices.Add( vertices[vertexIndex].z );
            }
            else if ( projectSide == 1 )
            {
                tempVertices.Add( vertices[vertexIndex].x );
                tempVertices.Add( vertices[vertexIndex].z );
            }
            else
            {
                tempVertices.Add( vertices[vertexIndex].x );
                tempVertices.Add( vertices[vertexIndex].y );
            }
        }

        Vector3 GetNormal( int vIdx0, int vIdx1, int vIdx2 )
        {
            Vector3 v0 = new Vector3(vertices[vIdx0].x, vertices[vIdx0].y, vertices[vIdx0].z);
            Vector3 v1 = new Vector3(vertices[vIdx1].x, vertices[vIdx1].y, vertices[vIdx1].z);
            Vector3 v2 = new Vector3(vertices[vIdx2].x, vertices[vIdx2].y, vertices[vIdx2].z);
            return Vector3.Normalize( Vector3.Cross( v1-v0, v2-v0 ) );
        }

        List<object> GetMaterialIndices( GeometryType geomType, List<object> values )
        {
            int depth = 0;
            switch (geomType)
            {
                case GeometryType.Solid:
                    depth = 1;
                    break;

                case GeometryType.MultiSolid:
                case GeometryType.CompositeSolid:
                    depth = 2;
                    break;
            }
            for ( int i = 0; i < depth; i++ )
            {
                values = values[0] as List<object>;
            }
            return values;
        }

        static long ToIndex( object o )
        {
            return o switch
            {
                int     => (int)o,
                short   => (short)o,
                long    => (long)o,
                _ => -1
            };
        }

        static float ToFloat( object o)
        {
            return o switch
            {
                float   => (float)o,
                double  => (float)(double)o,
                decimal => (float)(decimal)o,
                _ => 0
            };
        }

        static T TryGet<T>( Dictionary<string, object> data, string key, T defaultValue )
        {
            if ( data.TryGetValue(key, out object o ) )
            {
                if (o is T)
                    return (T)o;
            }
            return defaultValue;
        }

        static Vector3 TryGetVector3( Dictionary<string, object> data, string key, Vector3 defaultValue )
        {
            if (data.TryGetValue( key, out object o ))
            {
                var values = o as List<object>;
                if ( values != null && values.Count==3 )
                {
                    return new Vector3( ToFloat( values[0] ), ToFloat( values[1] ), ToFloat( values[2] ) );
                }
            }
            return defaultValue;
        }
    }
}