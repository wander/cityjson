## CityJson

Loads a cityjson (*.json or *.city.json) file and provides acces to its
vertices, appearance, materials, etc.

### No dependencies to Unity!
- This can se used in any C# project.

### Usage:

```
var json = File.ReadAllText(path_to_json);
var cj   = new CityJson( json );

// Each geometry represents a subset in a mesh.
cj.Geometries

// Materials
cj.Materials

// Vertices
var unityVertices = cj.Vertices.Select( v => new Vector3(v.x, v.z, v.y) ).ToArray();

for (int i=0; i<cj.CityObjects.Count;i++ )
{
    var geom = cj.CityObjects[i].geometry;
    Mesh m = new Mesh();
    m.indexFormat  = UnityEngine.Rendering.IndexFormat.UInt32;
    m.subMeshCount = geom.indices.Count;
    m.SetVertices( unityVertices );
    for ( int j=0; j<geom.indices.Count; j++)
    {
	    m.SetTriangles( geom.indices[j], j );
	}
    m.RecalculateNormals();
    m.Optimize();
    m.UploadMeshData( true );
}

```

### Important:
CityJson provides geometry in very small pieces (very few vertices) usually. This is dramatic for rendering performance. The provided data must be merged on a per material basis or any other criteria before used used on the graphics card when working with large data files.